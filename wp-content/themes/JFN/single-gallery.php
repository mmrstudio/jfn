<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

get_header(); ?>

   
	<div class="container">
		<div class="row">
			<div class="col-md-12">
     <h2 class="widget-title">  <?php echo the_title(); ?> </h2>

           <?php 

$images = get_field('images');
if( $images ): ?>
    <ul>
    
        <?php foreach( $images as $image ): ?>
                <div class="col-md-3">
                <a href="<?php echo $image['url']; ?>">
                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                </a>
               </div>
               <?php $counter++; ?>
        <?php endforeach;
       
    ?> </ul>
<?php endif; ?>
	</div><!-- end row -->
</div><!-- end of .container -->

</div>
<?php get_footer(); ?>
