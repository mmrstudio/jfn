<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/*
 Template Name: Home
 */

//get_header('home');
get_header('');
 ?>

<?php // get_template_part( 'includes/home-panels' );

 ?>

<div class="content-body">
		
 <div class="main-category">

 	<span> <?php echo get_field('home_page_text','option'); ?></span>
 	<div class="yellow-border"></div>
  	<?php echo get_field('home_page_sub_text','option'); ?> 

	  <div class="choose">
	  	<?php $section = get_field('sections','option');

	  	foreach($section as $sections) { ?>

	  		<div class="box col-sm-12 col-xs-12">
	  			<div class="col-sm-3 col-xs-3"><img src="<?php echo $sections['icon']; ?>"></div>
	  			<div class="col-sm-9 col-xs-9 name"> <a href="<?php echo $sections['page']; ?>"> <?php echo $sections['name']; ?> </a> </div>
	  		</div>

	  	<?php } ?>
	  
	  </div>
	 <div class=""> <a href="#mydiv" class="wplightbox" data-width=800 data-height=400 title="Inline Div">How it works</a></div>
 <div id="mydiv" style="display:none;">
  <div class="lightboxcontainer">
	<div class="lightboxleft">
	  <div class="divtext">
		<p class="divtitle" style=" ">How it works</p>
		<div class="divdescription">
		<p class="" style="font-size:14px;line-height:20px;">
			<?php echo get_field('pop_up','option');?>

</div>
	  </div>
	</div>
	 
	<div style="clear:both;"></div>
</div></div>

 </div>


 
<div class="corner-image"> <img src="<?php bloginfo('stylesheet_directory'); ?>/core/images/overlay.png"> </div>		

</div> 



<?php  get_footer(); ?>
