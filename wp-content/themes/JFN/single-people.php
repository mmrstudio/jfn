<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

get_header(); ?>


<div class="container players-single">
	<div id="content-wrap" class="row">
		<div class="col-md-12">
			<?php  get_template_part( 'loop-header' ); ?>
		</div>
		<div class="col-md-9 post-content">
			<?php if (have_posts()) : ?>
			    <?php while (have_posts()) : the_post(); ?>
			    	<h1 class="news-title"><?php the_title(); ?></h1>


					<div class="player-img-container">
						<?php 
						$player_image = get_field('player_image');
						if( $player_image ) {
							echo wp_get_attachment_image( $player_image, 'player-img' );
						}else{ ?>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/core/images/player.jpg" alt="">
						<?php } ?>
					</div>

					<div class="player-stats-table">						
						<table>
							<?php if(get_field('height')){ ?><tr><td class="l-td">HEIGHT:</td><td><?php the_field('height'); ?></td></tr><?php } ?>
							<?php if(get_field('weight')){ ?><tr><td class="l-td">WEIGHT: </td><td><?php the_field('weight'); ?></td></tr><?php } ?>
							<?php if(get_field('jnr_club')){ ?><tr><td class="l-td">JNR CLUB: </td><td><?php the_field('jnr_club'); ?></td></tr><?php } ?>
						</table>
					</div>
			        <?php the_content(); ?>
			    <?php endwhile; ?>
			    <?php else : ?>
			    	<p>No Posts.</p>
			<?php endif; ?>
			
			<?php get_template_part( 'includes/next-prev' ); ?>

		</div>

			<?php get_sidebar(); ?>

	</div>
</div>

<?php //get_template_part( 'includes/sponsors' ); ?>

<?php get_footer(); ?>
