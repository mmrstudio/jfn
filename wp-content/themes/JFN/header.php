<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap" rel="stylesheet">
    
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	 <div id="chat-icon">
			<a href="https://nchat.joefortune.com/iframe/iframe.html?lin=false&ftv=false&ftd=false&df=false&lang=en"><img src="<?php bloginfo('stylesheet_directory'); ?>/core/images/chat.svg"></a>
	</div>

		<header id="masthead" class="website-header">

			<div class="main-logo">	
				<a href="<?php echo home_url(); ?>" >
					<img src="<?php echo get_field('site_logo','option'); ?>"> 
				</a>
			</div>

			<div id="nav-container" >
	<div class="menu">
	<?php wp_nav_menu( array(
					'container'       => 'div',
					'container_class' => 'main-nav',
					'theme_location'  => 'top-menu',
					'menu'  => 'Top Menu'
				)
			);
	?>
	</div>
</div>
</header><!-- #masthead -->
 
<div id="content" class="site-content">
 