
<div class="fixture-ladder">
<div class="fixture-back">
<div class="container">
<?php 
      $leagues = get_field('fixtures_results', 'option');
      $fixtures_counter = 100;
      $fixtures_counter++;
       // print_r($leagues);
  ?>
<div class="col-md-12 fix-head">

    <div class="col-md-6 fix-head">
        <h1> Competition</h1>
    </div>
	<div class="col-md-6 comp-drop">
	<section class="widget_match_center_heading"> 
	                                             
					 <?php   $count2= 0; ?>
	       <select name="sblock<?php echo $fixtures_counter; ?>3_dropdown_name" class="SP_ext_scripts unstyled selectMe2"  id="sblock<?php echo $fixtures_counter; ?>3_dropdown">                
	              <?php
	                      foreach($leagues as $league2 ) {
	                                                       
	                         $count2++;
	                        $selectcounter++; 
	                        $comp_name =  str_replace('/', "-", $league2['competition_name']);
	                        $comp_name = stripslashes($comp_name);
	                        $comp_name = preg_replace('/[^A-Za-z0-9\-]/', '',  $comp_name);?>
	                                                     

	                 <option value="<?php echo $comp_name; ?>" data-group='<?php echo $league['event_year'] ; ?>'><?php echo $league2['competition_name']; ?></option>
	                       <?php 
	                         }
	                        ?>
	             </select>
	</section>  
	</div> 
</div>
 <?php 
    $rows = get_field('fixtures_results', 'option');
	if($rows){          
            $fixtures_counter = 0;
            foreach($rows as $league3) { 

                    $fixtures_counter++;
                    $comp_name3 =  str_replace('/', "-", $league3['competition_name']); 
                    $comp_name3 = stripslashes($comp_name3);
                    $comp_name = preg_replace('/[^A-Za-z0-9\-]/', '',  $comp_name3);?> 
  <div class="comp-background">                  
<div class="col-md-12 comp-panel">
<div class="col-md-7 fix-results"> 
    <div id="<?php echo $comp_name;?>" class=" group">
		<div class="results_container">
                <section class="widget_match_center_heading"></h2>
                <select style="display:none" title="sblock0<?php echo $fixtures_counter; ?>3_dropdown_name" name="sblock<?php echo $fixtures_counter; ?>3_dropdown_name" class="SP_ext_scripts" id="sblock0<?php echo $fixtures_counter; ?><?php echo $i;?>3_dropdown">                 
                    <option label="sblock0<?php echo $fixtures_counter; ?><?php echo $i;?>3_dropdown_name" value=""></option>                 
                                        <option label="sblock0<?php echo $fixtures_counter; ?>3_dropdown_name" value="http://websites.sportstg.com/ext/external_schedule.cgi?c=<?php echo $league3['competition_id']; ?>&pool=<?php echo $league3['time_of_season']; ?>&round=<?php echo $league3['round_number']; ?>&cols=2&fix=0&json=1"> State Championship Men </option>
                                </select> 
                              </section>   

                           

                                <section id="sblock0<?php echo $fixtures_counter; ?>3_wrapper" name="sblock0<?php echo $fixtures_counter; ?><?php echo $i;?>3_dropdown_name">
                              </section>

                       
                        </div>
                        </div>
                      </div> 
              <div class="col-md-5">
					<div class="group" id="<?php echo $comp_name;?>">
           <div class="results_container">
          <section class="widget_match_center_heading">
            
                <select style="display:none" name="sblock5<?php echo $fixtures_counter; ?>0_dropdown_name" class="SP_ext_scripts" id="sblock5<?php echo $fixtures_counter; ?>0_dropdown">             
                  <option value=""></option>                 
                  <option value="http://websites.sportstg.com/ext/external_ladder.cgi?c=<?php echo $league3['competition_id']; ?>&pool=-1&round=0&pool=-<?php echo $fix2['time_of_season'] ; ?>&cols=1,2,3,4,8&fix=0&json=1"><?php echo $league3['competition_name']; ?></option>
             </select> 
          </section>  
               <div class="ladder-title">
                Ladder
                </div>
                                
              <section id="sblock5<?php echo $fixtures_counter; ?>0_wrapper" name="sblock5<?php echo $fixtures_counter; ?>0_dropdown_name" class="sblock">
                </section>

                <div class="full-ladder">
                <a href="<?php echo get_field('full_fixture_&_results_link', 'option');?>" target="_blank"> full fixtures & Results</span></a>
                </div>
         </div>

        </div> 
					</div>
					<!-- End ladder_container -->
					</div>
	</div> <!-- End comp-background-->
                             <?php  } }?>  
           
<div class="col-md-12 view-all-fixture"> <h3 class="block-btn"><a href="<?php echo get_field('all_fixture_&_results_link', 'option');?>" class="blue"> All Fixtures & Results</a></h3> </div>

</div> 
</div>
</div>
<div class="container get-involved-find-club">

<h1> Get Involved</h1>
<div class="col-md-4"> <!-- AFL Logos  -->
<div class="get-involved">
<img src="<?php echo get_field('main_logo','option');?>">


<?php
$rows = get_field('sub_logos','option');
if($rows)
{
	echo '<ul class="get-involved-logos clearfix">';

	foreach($rows as $row)
	{
		echo '<li><a href="'.$row['link'].'"><img src="'.$row['image'].'" /> </a>';
	}

	echo '</ul>';
} 
echo get_field('text','option');
?>

</div>
</div>

<div class="col-md-8"> <!-- club finder  -->
<div class="find-a-club-section">
		<h1>Find a Club</h1>
		<form class="clearfix" action="<?php echo home_url(); ?>/club-finder/" method="get">
			<input id="" name="search-value" placeholder="Enter a suburb or postcode" type="text">
			<button type="submit">R</button>
		</form>
		<h3>or complete the expression<br>of interest form, <a href="<?php echo get_field('expression_of_interest_form','option');?>">click here</a></h3>
	</div>
</div>

</div>
<div class="container">

<div class="col-md-12 boxes">
<ul class="get-involved-buttons clearfix">
<li> <a href="<?php echo get_field('box1_link','option'); ?>"><img src="<?php echo get_field('box1_image','option'); ?>" /><span><?php echo get_field('box1_text','option'); ?> </span> </a></li>
<li> <a href="<?php echo get_field('box2_link','option'); ?>"><img src="<?php echo get_field('box2_image','option'); ?>" /> <span><?php echo get_field('box2_text','option'); ?> </span> </a></li>
<li> <a href="<?php echo get_field('box3_link','option'); ?>"><img src="<?php echo get_field('box3_image','option'); ?>" /> <span><?php echo get_field('box3_text','option'); ?> </span> </a></li>
</ul>



</div>
</div>

  <?php  $title = get_field('panel_title','option'); 
  $comp_title = get_field('competition_title','option'); 
  $theme_color = get_field('theme_color','option'); 
  $theme_font = get_field('theme_font_color','option'); 
  $round = get_field('round_background','option'); 
  $button_bg = get_field('button','option'); 
  $arrow_back = get_field('arrow_background_gn','option');
  $arrow = get_field('arrow_gn','option');
  ?>
<style type="text/css">
  
  .fixture-ladder h1 {
    color:<?php echo $comp_title; ?>;
  }
  .spfixture .spcompname {
      background: <?php echo $theme_color;?>;
      color:<?php echo $title; ?> ;
  }
  .spfixture .sproundname {
    background:<?php echo $round; ?> ;
  }
  .ladder-title {
    background: <?php echo $theme_color;?>;
    color:<?php echo $title; ?> ;
  }
  .block-btn .blue {
    background:<?php echo $button_bg;?>;
  }
  .get-involved-buttons span {
    background: <?php echo $theme_color;?>;
    color:<?php echo $theme_font; ?> ;
  }
  .get-involved-buttons li a span:before {
    background: <?php echo $arrow_back; ?>;
    color: <?php echo $arrow; ?>;
  }

</style>


<?php $leagues = get_field('fixtures_results', 'option');
               
      if($leagues) {   
          foreach($leagues as $league2 ) {
                                                       
                    $count2++;
                    $selectcounter++; 
                    $comp_name =  str_replace('/', "-", $league2['competition_name']);
                    $comp_name = stripslashes($comp_name);
                    $comp_name = preg_replace('/[^A-Za-z0-9\-]/', '',  $comp_name);?>
            <script type="text/javascript">

   jQuery(document).ready(function($) {
      
  $('div.group').hide();
  $('div#<?php echo $comp_name;?>').show();
  $('.selectMe2').change(function () {  
    	  
  $('div.group').hide();
       var fix= this.value; //alert(fix);
        var fix= 'div#' + fix;
  $(fix).show();
   }); 

 
 });
   
</script>     
       <?php   
 return;
      }}
   ?>


</div>

