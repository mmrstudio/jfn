<div class="mega-menu clearfix">
	
	<div class="container">
		<div class="row">

		<?php 
		// wp_nav_menu( array(
		// 		'container'       => 'div',
		// 		'container_class' => 'test',
		// 		'theme_location'  => 'header-menu',
		// 		'menu'     => 'Top Menu'
		// 	)
		// );
		?>

		<div class="col-md-3 first">
			<?php

			// check if the flexible content field has rows of data
			if( have_rows('column_one__contents', 'option') ):

			     // loop through the rows of data
			    while ( have_rows('column_one__contents', 'option') ) : the_row();

			        if( get_row_layout() == 'heading' ): 
			        	$hide_in_footer = get_sub_field('hide_in_footer'); ?> 
			        		<h2 class="hide-in-footer<?php echo $hide_in_footer; ?>">
			        			<?php the_sub_field('heading') ?>
			        		</h2>
			        	<?php

			        elseif( get_row_layout() == 'menu' ):
			        	$hide_in_footer = get_sub_field('hide_in_footer');
						echo '<div class="hide-in-footer'.$hide_in_footer.'">';
			        	wp_nav_menu( array(
			        			'container'       => 'div',
			        			'container_class' => 'mega-menu-menu',
			        			'menu_class'      => 'mm-menu',
			        			'menu'     =>  get_sub_field('menu')
			        		)
			        	);
			        	echo '</div>';
			        	elseif( get_row_layout() == 'image' ):

			        	$image = get_sub_field('image');
			        	if(get_sub_field('external_link')){
			        		$url = get_sub_field('external_link');
			        		$target = 'target="_blank"';
			        	}else{
			        		$url = get_sub_field('page_link');
			        		$target = null;
			        	}
			        	echo '<a href="'. $url.'"'.$target.'><img class="img-responsive" src="' . $image['url'] . '" alt="' . $image['alt'] . '" /></a>';

			        
			        endif;

			    endwhile;

			else :

			    // no layouts found

			endif;

			?>
		</div>

		<div class="col-md-2 second">
			<?php

			// check if the flexible content field has rows of data
			if( have_rows('column_two__contents', 'option') ):

			     // loop through the rows of data
			    while ( have_rows('column_two__contents', 'option') ) : the_row();

			        if( get_row_layout() == 'heading' ):
			        $hide_in_footer = get_sub_field('hide_in_footer');  ?>
						<h2 class="hide-in-footer<?php echo $hide_in_footer; ?>">
							<?php the_sub_field('heading') ?>
						</h2>
					<?php
			        elseif( get_row_layout() == 'menu' ):
			        	$hide_in_footer = get_sub_field('hide_in_footer');
						echo '<div class="hide-in-footer'.$hide_in_footer.'">';
			        	wp_nav_menu( array(
			        			'container'       => 'div',
			        			'container_class' => 'mega-menu-menu',
			        			'menu_class'      => 'mm-menu',
			        			'menu'     =>  get_sub_field('menu')
			        		)
			        	);
			        	echo '</div>';

			        endif;

			    endwhile;

			else :

			    // no layouts found

			endif;

			?>
		</div>
		<div class="col-md-2 third">
			<?php

			// check if the flexible content field has rows of data
			if( have_rows('column_three__contents', 'option') ):

			     // loop through the rows of data
			    while ( have_rows('column_three__contents', 'option') ) : the_row();

			        if( get_row_layout() == 'heading' ):
			        $hide_in_footer = get_sub_field('hide_in_footer');  ?>
						<h2 class="hide-in-footer<?php echo $hide_in_footer; ?>">
							<?php the_sub_field('heading') ?>
						</h2>
					<?php

			        elseif( get_row_layout() == 'menu' ):
			        	$hide_in_footer = get_sub_field('hide_in_footer');
						echo '<div class="hide-in-footer'.$hide_in_footer.'">';
			        	wp_nav_menu( array(
			        			'container'       => 'div',
			        			'container_class' => 'mega-menu-menu',
			        			'menu_class'      => 'mm-menu',
			        			'menu'     =>  get_sub_field('menu')
			        		)
			        	);
			        	echo '</div>';

			        endif;

			    endwhile;

			else :

			    // no layouts found

			endif;

       
			?>
		</div>
		
		<div class="col-md-2 fourth">
			<?php

			// check if the flexible content field has rows of data
			if( have_rows('column_four__contents', 'option') ):

			     // loop through the rows of data
			    while ( have_rows('column_four__contents', 'option') ) : the_row();

			        if( get_row_layout() == 'heading' ):
			        $hide_in_footer = get_sub_field('hide_in_footer');  ?>
						<h2 class="hide-in-footer<?php echo $hide_in_footer; ?>">
							<?php the_sub_field('heading') ?>
						</h2>
					<?php

			        elseif( get_row_layout() == 'menu' ):
			        	$hide_in_footer = get_sub_field('hide_in_footer');
						echo '<div class="hide-in-footer'.$hide_in_footer.'">';
			        	wp_nav_menu( array(
			        			'container'       => 'div',
			        			'container_class' => 'mega-menu-menu',
			        			'menu_class'      => 'mm-menu',
			        			'menu'     =>  get_sub_field('menu')
			        		)
			        	);
			        	echo '</div>';
			        elseif( get_row_layout() == 'image' ):

			        	$image = get_sub_field('image');
			        	if(get_sub_field('external_link')){
			        		$url = get_sub_field('external_link');
			        		$target = 'target="_blank"';
			        	}else{
			        		$url = get_sub_field('page_link');
			        		$target = null;
			        	}
			        	echo '<a href="'. $url.'"'.$target.'><img class="img-responsive" src="' . $image['url'] . '" alt="' . $image['alt'] . '" /></a>';

			        endif;

			    endwhile;

			else :

			    // no layouts found

			endif;

			?>
		</div>
		<div class="col-md-2 fifth">
			<?php

			// check if the flexible content field has rows of data
			if( have_rows('column_five__contents', 'option') ):

			     // loop through the rows of data
			    while ( have_rows('column_five__contents', 'option') ) : the_row();

			        if( get_row_layout() == 'heading' ):
			        $hide_in_footer = get_sub_field('hide_in_footer');  ?>
						<h2 class="hide-in-footer<?php echo $hide_in_footer; ?>">
							<?php the_sub_field('heading') ?>
						</h2>
					<?php

			        elseif( get_row_layout() == 'menu' ):
			        	$hide_in_footer = get_sub_field('hide_in_footer');
						echo '<div class="hide-in-footer'.$hide_in_footer.'">';
			        	wp_nav_menu( array(
			        			'container'       => 'div',
			        			'container_class' => 'mega-menu-menu',
			        			'menu_class'      => 'mm-menu',
			        			'menu'     =>  get_sub_field('menu')
			        		)
			        	);
			        	echo '</div>';
			        elseif( get_row_layout() == 'image' ):

			        	$image = get_sub_field('image');
			        	if(get_sub_field('external_link')){
			        		$url = get_sub_field('external_link');
			        		$target = 'target="_blank"';
			        	}else{
			        		$url = get_sub_field('page_link');
			        		$target = null;
			        	}
			        	echo '<a href="'. $url.'"'.$target.'><img class="img-responsive" src="' . $image['url'] . '" alt="' . $image['alt'] . '" /></a>';

			        endif;

			    endwhile;

			else :

			    // no layouts found

			endif;

			?>
		</div>

		</div>
	</div>
	



</div>