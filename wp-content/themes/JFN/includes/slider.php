
<?php echo "panell test";
//echo get_field('site_specific_css','option'); 
	$args = array(
		'post_type' => 'post',
		'category_name' => 'slider',
		'posts_per_page' => '5'
	);
	$the_query = new WP_Query( $args );
	if ( $the_query->have_posts() ) {
?>
<div class="slick-slider-container">
	
	<div class="mv-slider ">
	<?php 
		global $seen_posts;
		$seen_posts = array();
		while ( $the_query->have_posts() ) {
		$the_query->the_post();
		$seen_posts[] = $post->ID;
			if ( has_post_thumbnail()) {
				$thumb_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
				$slider_img = $thumb_image_url[0];
			 } else { 
			 	if (getFeaturedVideoPreview($post->ID) !="") {
			 		$yt_url = get_post_meta($post->ID, 'featuredVideoURL', true);
			 		$slider_img_part = substr( $yt_url, strrpos( $yt_url, '=' )+1 );
			 		$slider_img = 'http://img.youtube.com/vi/'.$slider_img_part.'/hqdefault.jpg';
			 	}else{
			 		$slider_img = get_stylesheet_directory_uri().'/core/images/placeholder.jpg';
			 	}
			 	?>
		  	<?php } ?>
			  <li class="mv-slide">
			  	<img class="post-image" src="<?php echo $slider_img; ?>">
			  	  	<div class="caption">
			  	  		<h2><a href="<?php the_permalink() ?>"><span><?php echo ShortenText( 58, get_the_title(), false ); ?></span></a></h2>
			  	  		<div class="slider-caption-wrapper clearfix">
			  	  			
			  		  		<div class="slider-caption-bg clearfix">
			  		  			<p><?php echo get_excerpt( 120 ); ?></p>
							
			  		  		</div>

			  		  		<div class="slider-caption-date">
			  		  			<div class="slider-date">
				  			  		<?php $post_date = get_the_date('F j,Y'); 
				  			  		echo $post_date; ?>
				  			  	</div>	
			  					<ul class="social-search-list clearfix">
			<?php 
			if(get_field('facebook', 'option') != "") { ?>
				<li><a href="https://www.facebook.com/<?php echo get_field('facebook', 'option');?>" target="_blank">f</a></li>
			<?php }
			?>
				<?php 
			if(get_field('twitter', 'option') != "") { ?>
				<li><a href="https://twitter.com/<?php echo get_field('twitter', 'option');?>" target="_blank">t</a></li>
			<?php }
			?>
				<?php 
			if(get_field('you_tube', 'option') != "") { ?>
				<li><a href="https://www.youtube.com/channel/<?php echo get_field('you_tube', 'option');?>" target="_blank">y</a></li>
			<?php }
			?>


				</li>
			</ul>
			  					<a class="full-story" href="<?php the_permalink(); ?>"><div class="slider-full-story">Full Story</div></a>
			  			  	</div>


			  		  		<a class="slider-arrow" href="<?php the_permalink(); ?>"><div class="slider-caption-arrow"></div></a>		  			  	
			  		  	</div>	
			  	  	</div>
			  </li>
			<?php } ?>

			  	 

	</div>
	
	<div class="slider-nav-container ">
		<div class="mv-slider-nav">
		<?php
		  while ( $the_query->have_posts() ) {
		  		$the_query->the_post();
		  			if ( has_post_thumbnail()) {
		  				$thumb_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'thumbnail');
		  				$slider_img = $thumb_image_url[0];
		  			 } else { 
		  			 	if (getFeaturedVideoPreview($post->ID) !="") {
		  			 		$yt_url = get_post_meta($post->ID, 'featuredVideoURL', true);
		  			 		$slider_img_part = substr( $yt_url, strrpos( $yt_url, '=' )+1 );
		  			 		$slider_img = 'http://img.youtube.com/vi/'.$slider_img_part.'/hqdefault.jpg';
		  			 	}else{
		  			 		$slider_img = get_stylesheet_directory_uri().'/core/images/placeholder.jpg';
		  			 	}
		  			 	?>
		  		  	<?php } ?>
		  			  <div class="mv-slide">
		  			  	
		  			  		<img class="post-image1" src="<?php echo $slider_img; ?>">
		  			  		<div class="slide-thumb-content">
		  			  			
		  			  			<h2><?php echo ShortenText( 58, get_the_title(), false ); ?></h2>

		  			  		</div>
		  			  
		  			  </div>
		  			<?php } 
		  			wp_reset_postdata();
		  			?>
		</div>
	</div>

</div>
<?php } ?>

<?php $slider_background = get_field('title_background','option');
 $title = get_field('title_color','option');
 $arrow_bg = get_field('slider_arrow_background','option'); 
 $theme_color = get_field('theme_color','option');?>
<style type="text/css">
  .slick-slider-container .saafl-slider li .caption .caption-header {
    background: <?php echo $slider_background; ?>;
  }
  .slick-slider-container .mv-slider li .caption h2:after {
  	color:<?php echo $title;?>;
  }
  .slick-slider-container .mv-slider li .caption h2 span {
    background: <?php echo $theme_color;?>;
    color:<?php echo $title; ?>;
  }
  .slick-slider-container .mv-slider li .slider-arrow {
    background:<?php echo $arrow_bg;?> ;
   }
   .slider-nav-container li.slick-active {
    background:<?php echo $theme_color;?>;
   }
   .slick-slider-container .mv-slider-nav .mv-slide a, .mv-slider-nav .slick-slide {
   	background:<?php echo $theme_color;?>;
   }
   .slick-slider-container .mv-slider-nav .mv-slide h2 {
   	color:<?php echo $title; ?>;
   }
</style>

<script>
	jQuery(document).ready(function($) {
		$('.mv-slider').slick({
		 // centerMode: true,
		//  variableWidth: true,
		  // centerPadding: '50px',
		  slidesToShow: 1,
		  autoplay: false,
		  autoplaySpeed: 3000,
		  infinite: true,
		  slide: 'li',
		  asNavFor: '.mv-slider-nav',
		 
  		 
		  responsive: [
		    {
		      breakpoint: 769,
		      settings: {
		      //  centerMode: true,
		        centerPadding: '0',
		        slidesToShow: 1,
		        variableWidth: false
		      }
		    },
		    {
		      breakpoint: 760,
		      settings: {
		        centerMode: true,
		        centerPadding: '0',
		        slidesToShow: 1,
		        variableWidth: false,
		        adaptiveHeight: false,
		        touchThreshold: 10
		      }
		    }
		  ]
		});


		$('.mv-slider-nav').slick({
		  slidesToShow: 5,
		  slidesToScroll: 1,
		  asNavFor: '.mv-slider',
		  focusOnSelect: true,
		  centerMode: false,
		  vertical: true,
		  arrows: false
		});

		// $('.mv-slider-nav').find('.mv-slide').click(function(event) {
		// 	$('.mv-slider-nav').find('.mv-slide').removeClass('slick-clicked');
		// 	$(this).toggleClass('slick-clicked');

		// });

		// $('.slick-next').click(function(event) {
		// 	if($('.mv-slider-nav').find('.slick-clicked').is(':last-child')){
		// 		$('.mv-slider-nav').find('.slick-clicked').removeClass('slick-clicked');
		// 		$('.mv-slider-nav').find('.mv-slide[index="0"]').addClass('slick-clicked');
		// 	}else{
		// 		$('.mv-slider-nav').find('.slick-clicked').removeClass('slick-clicked').next().addClass('slick-clicked');
		// 	}
		// });

		// $('.slick-prev').click(function(event) {
		// 	if($('.mv-slider-nav').find('.slick-clicked').is(':first-child')){
		// 		$('.mv-slider-nav').find('.slick-clicked').removeClass('slick-clicked');
		// 		$('.mv-slider-nav').find('.mv-slide[index="4"]').addClass('slick-clicked');
		// 		console.log('last');
		// 	}else{
		// 		$('.mv-slider-nav').find('.slick-clicked').removeClass('slick-clicked').prev().addClass('slick-clicked');
		// 	}
		// });

	});
</script>