<div id="slider-container">
      <div class="container">
             
    <?php 
  $args = array(
    'post_type' => 'post',
    'category_name' => 'slider',
    'posts_per_page' => '5'
  );
  $the_query = new WP_Query( $args );
  if ( $the_query->have_posts() ) {
?>
<div class="slick-slider-container">
  <div class="row">
    <div class="col-md-8">
  <div class="saafl-slider">
  <?php 
       $count = 0;
    while ( $the_query->have_posts() ) {
    $the_query->the_post();
       if ( has_post_thumbnail()) {
                $thumb_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
                $slider_img = $thumb_image_url[0];
            
            } ?>
        <li class="saafl-slide">
       
          <input type="hidden" id="texens" name="user" value="<?php echo $slider_img; ?>" />
              <div class="caption">
                <div class="caption-header">
                  <h1>
                    <a href="<?php the_permalink() ?>">
                      <span class="skew"><?php echo get_the_title(); ?></span>
                    </a>
                </h1>
              </div>    
                <div class="slider-caption-wrapper clearfix">
                  <div class="slider-caption-sub clearfix"><p>
                            <?php $content = get_the_content();
                    $content = strip_tags($content);
                    echo substr($content, 0, 150)."..."; ?> 
                 
                    <?php //echo get_excerpt( 150 ); ?></p></div>
                </div>  
                <a class="slider-arrow" href="<?php the_permalink(); ?>"><div class="slider-caption-arrow"></div></a>
              </div>
        </li>
       <?php  $count++; 
              
       ?>
      <?php } ?>

  </div>
  </div>
  <div class="col-sm-4">
  <div class="slider-nav-container">
    <div class="saafl-slider-nav">
    <?php
      while ( $the_query->have_posts() ) {
          $the_query->the_post();?>
              <div class="saafl-slide">
      
                <div class="slide-thumb-content">
                    <div class="sub-heading"><?php echo  get_the_title(); ?></div>
                    <div><?php echo get_excerpt( 40 ); ?></div>
                </div>
           

              </div>
        <?php } ?>
    </div>
  </div>
    </div>
</div>
<?php } ?>
</div>

  </div>
</div>
 

<script>
  jQuery(document).ready(function($) {


    var opSlide = function(){
      var opSlider = $('.saafl-slider-nav');
      opSlider.on('init', function(event){
        //console.log('loded');
    // var theImg = $('.slick-active').find('img').attr('src');
        //var theImg = $("#texens").val();
   var theImg = $( '.slick-active' ).find('#texens').val();


        $('#slider-container').css('background-image', 'url(' + theImg + ')');
        $('#slider-container').css('background-size','cover');
        $('#slider-container').css('background-position','center top');

      });
      opSlider.on('afterChange', function(event, slick, currentSlide, nextSlide){
      // console.log('changed');
      //var theImg = $('.slick-active').find('img').attr('src');
     // var theImg = $("#texens").val();

 var theImg = $( '.slick-active' ).find( '#texens' ).val();


       $('#slider-container').css('background-image', 'url(' + theImg + ') ');
        $('#slider-container').css('background-size','cover');
        $('#slider-container').css('background-position','center top');

      });
    }


    $('.saafl-slider').slick({
      slidesToShow: 1,
    autoplay: true,
     autoplaySpeed: 3000,
      infinite: true,
      slidesToScroll: 1,
      slide: 'li',
      arrows: true,
      asNavFor: '.saafl-slider-nav',
      responsive: [
        {
          breakpoint: 769,
          settings: {
            centerMode: true,
            centerPadding: '0px',
            slidesToShow: 1,
            swipeToSlide: true,
            variableWidth: false,
            arrows: true
          }
        },
        {
          breakpoint: 370,
          settings: {
            arrows: true,
            centerMode: true,
            centerPadding: '0',
            slidesToShow: 1,
            variableWidth: false,
            adaptiveHeight: true,
            touchThreshold: 10
          }
        }
      ]
    });


    opSlide();

    $('.saafl-slider-nav').slick({
     <?php 
 $count2 = $count-1;
if($count++ < 6) {
        echo "slidesToShow:".$count2.",";
      } 
     else {

        echo  "slidesToShow:5,";
  }
      ?>
        slidesToScroll: 1,
        asNavFor: '.saafl-slider',
        dots: true,
        vertical: true,
        centerMode: false,
        arrows: true,
        focusOnSelect: true
    });



  });
</script>
<?php $slider_background = get_field('title_background','option');
 $title = get_field('title_color','option');
 $arrow_bg = get_field('slider_arrow_background','option'); 
 $theme_color = get_field('theme_color','option');?>
<style type="text/css">
  .slick-slider-container .saafl-slider li .caption .caption-header {
    background: <?php echo $slider_background; ?>;
  }
  .slick-slider-container .saafl-slider li .caption .caption-header h1 a {
    color: <?php echo $title;?>;
  }
   .slider-arrow {
    background:<?php echo $arrow_bg;?> ;
   }
   .slider-nav-container li.slick-active {
    background:<?php echo $theme_color;?>;
   }
</style>