<div class="container">
<ul class="col-md-12 ad-banners">


<?php
$banners = get_field('banner_images','option');

if($banners){
	foreach($banners as $banner)
	{
		echo "<li><a href=".$banner['link']."> <img src='".$banner['image']."' /> </a> </li>";
	}

}
?>
</div>

</ul>

<script>
	jQuery(document).ready(function($) {
		$('.ad-banners').slick({
		infinite: true,
		  slidesToShow: 3,
		  slidesToScroll: 1,
		  autoplay: true,
		  toplaySpeed: 2000,
		  responsive: [
		    {
		      breakpoint: 769,
		      settings: {
		        arrows: false,
		        centerMode: true,
		        centerPadding: '0px',
		        slidesToShow: 2,
		        infinite: true,
		  		slidesToScroll: 1,
		        // variableWidth: false
		      }
		    },
		     {
		      breakpoint: 480,
		      settings: {
		        arrows: false,
		        centerMode: true,
		        centerPadding: '0px',
		        slidesToShow: 2,
		        infinite: true,
		  		slidesToScroll: 2,
		        touchThreshold: 10,
		       
		      }
		    },
		    {
		      breakpoint: 479,
		      settings: {
		        arrows: false,
		        centerMode: true,
		        centerPadding: '0px',
		        slidesToShow: 1,
		        touchThreshold: 10
		      }
		    }
		  ]
		});

	});
</script>