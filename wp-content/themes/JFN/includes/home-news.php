<div class="container more-news-events arrow-bt">

		<div class="col-md-9 more-news-section">
			<h1><a href="<?php echo home_url(); ?>/news">More News</a></h1>
			<?php 

global $seen_posts;
?>
<?php
	$args = array(
		'post_type' => 'post',
		'posts_per_page' => 4,
		'category_name' => 'front-page-news',
		'post__not_in' => $seen_posts,
		'ignore_sticky_posts' => 1,
	);
	$the_query = new WP_Query( $args );
	if ( $the_query->have_posts() ) {
		echo '<ul class="front-more-news clearfix">';
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
?>

	<li>
		<a href="<?php the_permalink(); ?>">
			<?php
			if(has_post_thumbnail()){
				the_post_thumbnail('gallery-thumb');
			}else{ ?>
				<img src="<?php echo get_stylesheet_directory_uri() ?>/core/images/placeholder-square.jpg" alt="">
			<?php } ?>
			<h4><?php the_title(); ?></h4>
		</a>
		<div class="rel-article-time"><?php echo the_time('jS F, Y') ?></div>
		<p><?php echo get_excerpt( 200 ); ?></p>
		</li>


<?php 
	} // end while
		echo '</ul>';
wp_reset_postdata();
	} // end if
?>
		</div>
		<div class="col-md-3 event-sidebar">
			<?php if ( is_active_sidebar( 'events-sidebar' ) ) : ?>
				<?php dynamic_sidebar( 'events-sidebar' ); ?>
			<?php endif; ?>
		</div>
	
</div>
<?php 
	$news_title = get_field('news_title_color','option');
	$news_bg = get_field('news_background','option');
?>
<style type="text/css">
	.front-more-news li h4 {
		background: <?php echo $news_bg;?>;
    	color: <?php echo $news_title;?>;
	}
</style>